var index = require('../index.js');
var expect = require('chai').expect;

describe('Ejercicio 1 ¿Contiene arreglo?', function() {
    context('argumentos -> casa, ascuapl', function() {
      it('debería retornar -> true', function() {
        expect(index.containArray("casa", "ascuapl")).to.equal(true)
      })
    })
})

describe('Ejercicio 2 Números y letras', function() {
  context('argumentos -> estas3 espe4ro b6ien 1hola c2omo qu5e', function() {
    it('debería retornar -> 1hola c2omo estas3 espe4ro qu5e b6ien ', function() {
      expect(index.adjustTest('estas3 espe4ro b6ien 1hola c2omo qu5e')).to.equal("1hola c2omo estas3 espe4ro qu5e b6ien ")
    })
  })
})