const containArray = (palabra_uno, palabra_dos) => {
    let arr1 = palabra_uno.split("");
    let check = true;
    arr1.forEach(e => {
        if (palabra_dos.indexOf(e) == -1) {
            check = false;
        }
        palabra_dos = palabra_dos.replace(e, "");
    });
    return check;
}


const adjustTest = (cadena) => {
    let arrNum = [];
    let txtAll = "";
    let arr1 =  cadena.split(" ");

    for (i = 0; i < arr1.length; i++) {
        let num = arr1[i].match(/\d/g);
        num = num.join("");
        arrNum.push(num);
    }

    arrNum.sort((a, b) => {
        return a - b;
    });

    for (let j = 0; j < arrNum.length; j++) {
        const matches = arr1.filter(s => s.includes(arrNum[j]));
        txtAll += `${matches[0]} `;
    }
    return txtAll;
}

//const ok = adjustTest("estas3 espe4ro b6ien 1hola c2omo qu5e");
//console.log(ok);

//const coArr = containArray("casa", "ascuapl");
//console.log(coArr);

module.exports = { containArray, adjustTest }